# create-instance.tf

resource "aws_key_pair" "t" {

    key_name = "t"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC58yQRxIOmfacO2ZHp25PW9xYHn1UnIIrFUwXS11RT9hYh31inBN7P5o7/l/vqYKe/Wq/34jHpN0x+wtX7Qf7LAqO7bGcNlrlHQTA91oecf7dvoPvqNBHi9xSQkIJvbWyoI3hldzebGZ1vVHzXPK+E9yvH7RvsNI6pSX/CSz6Raoo/WDY7q7JrXsEnt+o2eCvXue04/QFop1n/5pKygGhY+FjwvOpmj96chACq4Mh0w4OY9qyeDzi4nVfW3ogCmIbBB+sg68lynCSavMJoHSbxih+EuQ3ceX43oKD0VC7d9VBsdewKAAxrCSzSna18lFVkdfzmznkSnjUk3zUZ1baJ2N6Iwki1PjEHR/iviFAiKv7CmX+YUFDG8Z26pfqS8gBNXroF5/KCtCcpjyz8gZxW/xvw2GEBu4UcIKPE18Gdk+Tgo53qopYZqzvm0mVsnC76UOQrPuY/973OpAD5zq8OKqme+F4+w0nQg6XFGFevwSKolEfznRJFbqRtbfkaHEM= sagar@LAPTOP-VQH5JBJU"
  
}

data "template_file" "temp" {
  template = file("template.tpl")

  vars = {
    
    
    aws_region = var.aws_region
  }
}
resource "aws_instance" "instance" {
  ami                         = var.instance_ami
  availability_zone           = "${var.aws_region}${var.aws_region_az}"
  instance_type               = var.instance_type
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.sg.id]
  subnet_id                   = aws_subnet.subnet.id
  key_name                    = "${aws_key_pair.t.key_name}"
  user_data = data.template_file.temp.rendered
 
 
  tags = {
    "Owner"               = var.owner
    "Name"                = "${var.owner}-instance"
    "KeepInstanceRunning" = "false"
  }
}